/// <reference types="Cypress"/>

const { Pages } = require("../page-objects/pages")
const { Contact } = require("../page-objects/contact")


describe('Menu', () => {
    const page = new Pages()
    const contactus = new Contact()
    let messageFixture

    before(function () {
        // A funcao fixture obtem os dados do arquivos json para utilizar nos testes.
        cy.fixture('messages').then((messagesData) => {
            messageFixture = messagesData
        })
    })


    it('homepage', () => {
        page.homepage()
    })

    it('login', () => {
        page.homepage()
    })

    it('contact', () => {
        page.contactus()
        contactus.verifyPage(messageFixture.contactUsTitle)
    })

    it('contact', () => {
        page.buy()
    })


})
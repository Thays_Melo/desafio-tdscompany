/// <reference types="Cypress"/>

const { Pages } = require("../../page-objects/pages")
const { Search } = require("../../page-objects/search")


describe('Busca', () => {
    const page = new Pages()
    const search = new Search()
    let messageFixture
    let searchFixture

    before(function () {
        // A funcao fixture obtem os dados do arquivos json para utilizar nos testes.
        cy.fixture('messages').then((messagesData) => {
            messageFixture = messagesData
        })

        cy.fixture('search').then((searchData) => {
            searchFixture = searchData
        })
    })

    beforeEach(() => {
        page.homepage()
    })

    it('campo de pesquisa', () => {
        search.searchField(searchFixture.search)     
    })


})
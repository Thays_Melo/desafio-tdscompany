const { Login } = require("../../page-objects/login")
const { Pages } = require("../../page-objects/pages")

describe('validando tela de login', () => {
  const login = new Login()
  const page = new Pages()
  let user
  let message

  before(function () {
    // Esta funcao fixture pega os dados do arquivo para utilizar nos testes.
    cy.fixture('user').then((userData) => {
      user = userData
    })

    cy.fixture('messages').then((messages) => {
      message = messages
    })
  })

  beforeEach(() => {
    page.login()
  })

  // Verifica o usuario nao acessa o sistema ao colocar um email invalido.
  it('login email incorreto', () => {
    login.login('user123email.com', user.password)
    login.incorrectEmail()
    
  })

  // Verifica o usuario nao acessa o sistema ao colocar uma senha invalida.
  it('login senha errado', () => {
    login.login(user.email, '12345')
    login.buttonLogin()
    login.incorrectPassword(message.incorretPassword)
  })

})

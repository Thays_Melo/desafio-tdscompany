const { Login } = require("../../page-objects/login")
const { Pages } = require("../../page-objects/pages")

describe('validando tela de recuperacao de senha', () => {
  const login = new Login()
  const page = new Pages()
  let user
  let message

  before(function () {
    // Esta funcao fixture pega os dados do arquivo para utilizar nos testes.
    cy.fixture('user').then((userData) => {
      user = userData
    })

    cy.fixture('messages').then((messages) => {
      message = messages
    })
  })

  beforeEach(() => {
    page.login()
  })

  // Verifica o usuario nao acessa o sistema ao colocar um email invalido.
  it('click recuperacao de senha', () => {
    login.buttonPasswordRecovery()
    login.verifyPagePassRecovery(message.recoveryPasswordTitle)
    login.passwordRecovery("adgr123email.com")
    login.buttonSubmitRecovery()
    login.invalidEmailRecovery(message.incorrectEmailRecovery)
    
  })

})

export class Contact {

    verifyPage(message) {
        cy.get('.page-subheading').should('have.text', message)
    }
}
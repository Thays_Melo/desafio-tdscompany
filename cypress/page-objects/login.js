export class Login {

    login(email, password) {
        cy.get('[id="email"]').type(email)
        cy.get('[id="passwd"]').type(password)
    }

    passwordRecovery(email) {
        cy.get('[id="email"]').type(email)
    }


    buttonLogin() {
        cy.get(':nth-child(2) > .box > .form_content > .submit > .button > span').click()
    }

    buttonPasswordRecovery(){
        cy.get('[href="http://automationpractice.com/index.php?controller=password"]').click()
    }

    buttonSubmitRecovery(){
        cy.get('.submit > .btn > span').click()
    }


    verifyPagePassRecovery(message) {
        cy.get('.page-subheading').should('have.text', message)
    }

    incorrectEmail() {
        cy.get('[id="email"]').should('have.css', 'background-color', 'rgb(255, 241, 242)')
    }

    incorrectPassword(message) {
        cy.get('.center_column > :nth-child(2)').should("contain.text", message)
    }

    invalidEmailRecovery(message){
        cy.get('.alert').should("contain.text", message)
    }


}
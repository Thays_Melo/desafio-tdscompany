export class Pages {
    homepage() {
        cy.visit('/index.php')
    }

    login() {
        cy.visit('/index.php?controller=authentication&back=my-account')
    }

    contactus() {
        cy.visit('/index.php?controller=contact')
    }

    buy() {
        cy.visit('/index.php?controller=order')
    }

}
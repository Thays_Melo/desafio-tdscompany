export class Search {
    searchField(search) {
        cy.get('[id="search_query_top"]').click().type(search)
        cy.get('#searchbox > .btn').click()
        cy.get(':nth-child(3) > .center_column').should('contain.text', search)
    }
}
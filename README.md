# Desafio Criativo TDS Company

Para a automação de testes foi sugerido utilizar o framework [Cypress](https://www.cypress.io/), que é destinado a automação de testes end-to-end.

## Instalação das bibliotecas e dependências

Em relação à importação dos scripts de automação dos testes, é necessário descompactar o arquivo do projeto **desafio-tdscompany**. Em seguida, acessar esta pasta e abrir o terminal ou prompt de comando e digitar o seguinte comando:

```sh
$ npm install cypress
```

Este comando irá instalar e/ou atualizar todas as dependências necessárias para utilização do framework.

Ao executar esta ação, é possível visualizar as pastas, subpastas e arquivos do cypress. No diretório **_Integration_** encontram-se todos os scripts de testes, e é este o local onde na execução do cypress, o framework irá buscar esses scripts.

## Configuração Global

O arquivo **_cypress.json_** é responsável por algumas configurações globais, a exemplo da url base utilizada em todos os testes:

```javascript
{
	"baseUrl":"http://automationpractice.com"
}
```

## Execução

Para executar o cypress, é necessário na raiz do diretório do projeto digitar um dos seguintes comandos:

```sh
$ ./node_modules/.bin/cypress open
```

ou

```sh
$ npm  run  cypress:open
```

Ao digitar um desses comandos, uma janela deverá ser exibida. Nesta janela é possível visualizar todos os scripts criados para a automação das funcionalidades dentro do sistema. Ao clicar em um dos scripts os testes são executados.

